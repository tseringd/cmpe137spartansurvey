//
//  PopUpController.swift
//  SpartanSurvey
//
//  Created by T D on 4/28/18.
//  Copyright © 2018 SJSU. All rights reserved.
//

import UIKit

class PopUpController: UIViewController {
    
    
    var alert = UIAlertController(title: "AlertController Tutorial", message: "Enter Title",preferredStyle: .alert)
    
    alert.addTextField { (textField: UITextField) in
    textField.keyboardAppearance = .dark
    textField.keyboardType = .default
    textField.autocorrectionType = .default
    textField.placeholder = "Enter title here"
    textField.clearButtonMode = .whileEditing
    }
    
    
    let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
        // Get 1st TextField's text
        let textField = alert.textFields![0]
        print(textField.text!)
    })
    
    let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
    
    
    alert.addAction(submitAction)
    alert.addAction(cancel)
    present(alert, animated: true, completion: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
