//
//  AlertViewController.swift
//  SpartanSurvey
//
//  Created by T D on 4/28/18.
//  Copyright © 2018 SJSU. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    var aSurvey = SurveyContent()
    
    @IBAction func fireAlert(_ sender: Any) {
        let alert = UIAlertController(title: "Spartan Survey", message: "Enter a title", preferredStyle: .alert)
        
        // Submit button
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
            // Get 1st TextField's text
            let textField = alert.textFields![0]
            print(textField.text!)
            
            self.aSurvey.title = textField.text!
            self.aSurvey.surveyType = "General"
            
            self.performSegue(withIdentifier: "toEditPage", sender: self)
            
        })
        
        
        // Cancel button
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        
        // Add 1 textField and cutomize it
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .dark
            textField.keyboardType = .default
            textField.autocorrectionType = .default
            textField.placeholder = "Enter Title here"
            textField.clearButtonMode = .whileEditing
            
        }
        
        
        // Add action buttons and present the Alert
        alert.addAction(submitAction)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.destination is SurveyContentViewController
        {
            let vc = segue.destination as? SurveyContentViewController
            vc?.survey = aSurvey
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
