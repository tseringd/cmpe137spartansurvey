//
//  AddQuestionViewController.swift
//  SpartanSurvey
//
//  Created by Peter Ghirmay on 4/29/18.
//  Copyright © 2018 SJSU. All rights reserved.
//

import UIKit

class AddQuestionViewController: UIViewController {
    
    var choiceCount: Int {
        get {
            var counter = 0
            if choiceOneField.text! != ""{counter = counter + 1}
            if choiceTwoField.text! != ""{counter = counter + 1}
            if choiceThreeField.text! != ""{counter = counter + 1}
            if choiceFourField.text! != ""{counter = counter + 1}

            return counter
        }
    }
    var aSurvey = SurveyContent()
    
    var aQuestion = Question()
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var questionField: UITextField!
    @IBOutlet weak var choiceOneField: UITextField!
    @IBOutlet weak var choiceTwoField: UITextField!
    @IBOutlet weak var choiceThreeField: UITextField!
    @IBOutlet weak var choiceFourField: UITextField!
    
    @IBAction func doneAdding(_ sender: UIButton) {
        var counter = 0
        if choiceOneField.text! != ""{counter = counter + 1}
        if choiceTwoField.text! != ""{counter = counter + 1}
        if choiceThreeField.text! != ""{counter = counter + 1}
        if choiceFourField.text! != ""{counter = counter + 1}
        
        if questionField.text! == "" {
            infoLabel.text = "Question Content is Required!"
        }
        else if counter < 2 {
            infoLabel.text = "Atleast Two Answer Choices Required!"
        }
        else {
            aQuestion.question = questionField.text!
            aQuestion.answers = [choiceOneField.text!, choiceTwoField.text!, choiceThreeField.text!,choiceFourField.text!]
            aSurvey.questions.append(aQuestion)
            self.performSegue(withIdentifier: "addQuestionToEditPage", sender: self)
        }
        
    }
    
    @IBAction func cancelAdding(_ sender: UIButton) {
        self.performSegue(withIdentifier: "addQuestionToEditPage", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.destination is SurveyContentViewController
        {
            let vc = segue.destination as? SurveyContentViewController
            vc?.survey = aSurvey
        }
    }
 

}
