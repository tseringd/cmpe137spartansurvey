//
//  SurveyContentViewController.swift
//  SpartanSurvey
//
//  Created by Yonas on 4/27/18.
//  Copyright © 2018 SJSU. All rights reserved.
//

import UIKit
import Firebase             //added for accessing firebase database
import FirebaseDatabase     //added for accessing firebase database

class SurveyContentViewController: UITableViewController{
    
    
    var survey = SurveyContent()
    var ref: DatabaseReference!     //instance of FIRDatabaseReference to read or write data from the database
    
    
    @IBOutlet weak var surveyHeader: UIView!
    
    @IBOutlet weak var surveyTitleView: UIView!
    @IBAction func addNewQuestion(_ sender: UIButton) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // survey.title = "survey1"
        
        self.tableView.isScrollEnabled = true
        self.tableView.separatorColor = UIColor.brown
        
        let codedLabel:UILabel = UILabel()
        codedLabel.frame = self.surveyTitleView.frame
        codedLabel.textAlignment = .center
        codedLabel.text = survey.title
        codedLabel.numberOfLines = 2
        codedLabel.textColor=UIColor.black
        codedLabel.font=UIFont.systemFont(ofSize: 20)
        codedLabel.backgroundColor=UIColor.cyan
        self.surveyTitleView.addSubview(codedLabel)
        
        /*
        let q1 = Question(aQuestion: "First Question", choices: ["red", "blue", "white"],aQuestionType: "single selection", isAnswerRequired: false)
        let q2 = Question(aQuestion: "Second Question", choices: ["square", "circle", "triangle"],aQuestionType: "single selection", isAnswerRequired: false)
        let q3 = Question(aQuestion: "third Question", choices: ["white", "green", "white"],aQuestionType: "single selection", isAnswerRequired: false)
        let q4 = Question(aQuestion: "fourth Question", choices: ["round", "oval", "triangle"],aQuestionType: "single selection", isAnswerRequired: false)
        let q5 = Question(aQuestion: "fifth Question", choices: ["apple", "banana", "orange"],aQuestionType: "single selection", isAnswerRequired: false)
        let q6 = Question(aQuestion: "sixth Question", choices: ["toyota", "nissan", "ferrarri"],aQuestionType: "single selection", isAnswerRequired: false)
        
        survey.questions.append(q1)
        survey.questions.append(q2)
        survey.questions.append(q3)
        survey.questions.append(q4)
        survey.questions.append(q5)
        survey.questions.append(q6)
        */
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
<<<<<<< HEAD
        //self.navigationItem.rightBarButtonItem = self.editButtonItem
||||||| merged common ancestors
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
=======
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
        // JOHN RAN'S CODE FOR SAVING TO DATABASE: The following code is for writing data to the firebase database
        var userSurvey = ["title": survey.title, "surveyType": survey.surveyType] as [String : Any]  //made the user's survey into a dictionary array
        var questionCount = 0
        var answerCount = 0
        for chosenQuestion in survey.questions {
            var oneQuestionDictionary = [String : Any]()
            oneQuestionDictionary["question"] = chosenQuestion.question
            answerCount = 0
            for answerChoice in chosenQuestion.answers {
                oneQuestionDictionary["answer_\(answerCount)"] = answerChoice
                answerCount += 1
            }
            oneQuestionDictionary["questionType"] = chosenQuestion.questionType
            oneQuestionDictionary["answerRequired"] = chosenQuestion.answerRequired
            userSurvey["question_\(questionCount)"] = oneQuestionDictionary
            questionCount += 1
        }
        ref = Database.database().reference()   //initializes the FIRDatabaseReference instance
        self.ref.child("surveys").setValue(userSurvey)  //set the value for the child "surveys" to the user's survey
>>>>>>> 862f75fe33a6ed14e2c20c52dcb9c1120c3614aa
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return survey.questions.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return survey.questions[section].answers.count
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return survey.questions[section].question
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        // Configure the cell...
        
        cell.textLabel?.text = survey.questions[indexPath.section].answers[indexPath.row]
        cell.backgroundColor = UIColor.cyan
        return cell
    }
    
    
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.destination is AddQuestionViewController
        {
            let vc = segue.destination as? AddQuestionViewController
            vc?.aSurvey = survey
        }
     }
    
}
