
__________________________________________________________________________________________
ID: 011215477
Name: Group 12
email Id: ygebremedhinpi@gmail.com

Description:

Login, Sign up and logout functionalities of the Spartan Survey app. App uses multiple views and view controllers connected by appropriate segues. Development environment done on Xcode Version 9.2 (9C40b) with swift 4. 

Dependencies:

Login and sign up is based on Firebase frame work that requires ‘Cocoapod’ installation on the development system or Firebase SDK. We chose the former as it makes framework implementations very easy by taking care of additional frameworks that Firebase might require. 

After configuring firebase and installing app pods on the desired project we used the .xcworkspace project file instead of the default .xcodeproj file to develop the desired functions of the app.
__________________________________________________________________________________________